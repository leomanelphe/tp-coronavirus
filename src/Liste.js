const Liste = (props) => {
    return (
        <ol>
            {props.data.Countries.map((country) => (
                <li>{country.Country}</li>
            ))}
        </ol>
    );
};

export default Liste;