import './App.css';
import Liste from './Liste';
import data from "./fakedata/data.json";

function App() {
  return <Liste data={data}/>;
}

export default App;
